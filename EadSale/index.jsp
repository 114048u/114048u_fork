<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>About - Car Repair Shop Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css">
	<![endif]-->
</head>
<body>
<jsp:include page="Header.jsp" />


	<div id="body">
		<div class="header">
			<ul>
				<li>
					<a href="services.html" class="figure"><img src="images/credit-card.png" alt=""></a>
					<div>
						<h3><a href="booking.html">affordable prices</a></h3>
						<p>
							INFORMATION
						</p>
					</div>
				</li>
				<li>
					<a href="services.html" class="figure"><img src="images/service.png" alt=""></a>
					<div>
						<h3><a href="services.html">fast service</a></h3>
						<p>
							INFORMATION
						</p>
					</div>
				</li>
				<li>
					<a href="services.html" class="figure"><img src="images/warranty.png" alt=""></a>
					<div>
						<h3><a href="booking.html">extensive warranty</a></h3>
						<p>
							INFORMATION
						</p>
					</div>
				</li>
			</ul>
		</div>
		<div class="body">
			<img src="images/engine.jpg" alt="">
			<div>
				<img src="images/BFC.jpg" width=""/>
				<a href="booking.html" style="margin-top: 216px;">book an appointment</a>
			</div>
		</div>
		<div class="footer">
			<div>
				<h2><span>featured services</span></h2>
				<ul>
					<li>
						<a href="services.html" class="figure"><img src="images/engine-maintenace.png" alt=""></a>
						<div>
							<h3><a href="services.html">engine maintenance</a></h3>
							<p>
								INFORMATION
							</p>
						</div>
					</li>
					<li>
						<a href="services.html" class="figure"><img src="images/condition-services.png" alt=""></a>
						<div>
							<h3><a href="services.html">air condition services</a></h3>
							<p>
								INFORMATION</p>
						</div>
					</li>
					<li>
						<a href="services.html" class="figure"><img src="images/wheel-alignment.png" alt=""></a>
						<div>
							<h3><a href="services.html">wheel alignment</a></h3>
							<p>
								INFORMATION</p>
						</div>
					</li>
					<li>
						<a href="services.html" class="figure"><img src="images/transmission-repair.png" alt=""></a>
						<div>
							<h3><a href="services.html">transmission repair and replacement</a></h3>
							<p>
								INFORMATION</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>
<div id="footer">
		<div>
			<div class="contact">
				<h3>contact information</h3>
				<ul>
					<li>
						<b>address:</b> <span>426 Grant Street colombo 10</span>
					</li>
					<li>
						<b>phone:</b> <span>0112222555</span>
					</li>
					<li>
						<b>fax:</b> <span>0112222555</span>
					</li>
					<li>
						<b>email:</b> <span><a href="http://www.bfc.com/misc/con">bfc@hotmail.com</a></span>
					</li>
				</ul>
			</div>
			<div class="tweets">
				<h3>recent tweets</h3>
				<ul>
					<li>
						<a href="#">Information technology Information technology<span>1 day ago</span></a>
					</li>
					<li>
						<a href="#">Information technology Information technology<span>2 days ago</span></a>
					</li>
				</ul>
			</div>
			<div class="posts">
				<h3>recent blog post</h3>
				<ul>
					<li>
						<a href="#">Information technology </a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology Information technology </a>
					</li>
				</ul>
			</div>
			<div class="connect">
				<h3>stay in touch</h3>
				<p>
					Information technology Information technology Information technology Information technology				</p>
				<ul>
					<li id="facebook">
						<a href="https://facebook.com">facebook</a>
					</li>
					<li id="twitter">
						<a href="https://twitter.com">twitter</a>
					</li>
					<li id="googleplus">
						<a href="https://google.com">googleplus</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="section">
			<p>
				&copy; BY MOROWING
			</p>
			<ul>
				<li>
					<a href="index.html">home</a>
				</li>
				<li>
					<a href="about.html">about</a>
				</li>
				<li>
					<a href="services.html">services</a>
				</li>
				<li>
					<a href="blog.html">blog</a>
				</li>
				<li>
					<a href="contact.html">contact</a>
				</li>
				<li>
					<a href="booking.html">book an appointment</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>