<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.List" %>
<%@ page import="com.google.appengine.api.users.User" %>
<%@ page import="com.google.appengine.api.users.UserService" %>
<%@ page import="com.google.appengine.api.users.UserServiceFactory" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html>

  <body>

<%
    UserService userService = UserServiceFactory.getUserService();
    User user = userService.getCurrentUser();
    if (user != null) {
      pageContext.setAttribute("user", user);
%>
<p>Hello, ${fn:escapeXml(user.nickname)}! (You can
<a href="<%= userService.createLogoutURL(request.getRequestURI()) %>">sign out</a>.)</p>
<%
    } else {
%>
<p>Hello!
<a href="<%= userService.createLoginURL(request.getRequestURI()) %>">Sign in</a>
to include your name with greetings you post.</p>
<%
    }
%>

<div id="header">
		<div>
			<a href="index.jsp" class="logo"><img src="images/car.png"  style="width: 208px;" alt=""></a>
			<form action="index.jsp">
				<input type="text" name="search" id="search" value="">
				<input type="submit" name="searchBtn" id="searchBtn" value="">
			</form>
		</div>
		<div class="navigation">
			<ul>
				<li class="selected">
					<a href="index.jsp">home</a>
				</li>
				<li>
					<a href="about.jsp">about</a>
					<ul>
						<li>
							<a href="team.jsp">the team</a>
						</li>
						<li>
							<a href="testimonials.jsp">testimonials</a>
						</li>
						<li>
							<a href="gallery.jsp">gallery</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="services.jsp">services</a>
					<ul>
						<li>
							<a href="services.jsp">engine maintenance</a>
						</li>
						<li>
							<a href="services.jsp">wheel allignment</a>
						</li>
						<li>
							<a href="services.jsp">air condition services</a>
						</li>
						<li>
							<a href="services.jsp">transmission</a>
						</li>
						<li>
							<a href="promo.jsp">promos &amp; discounts</a>
						</li>
					</ul>
				</li>
				<li>
					<a href="blog.jsp">select your car</a>
				</li>
				<li>
					<a href="contact.jsp">contact</a>
				</li>
				<li class="booking">
					<a href="booking.jsp"></a>
				</li>
			</ul>
		</div>
	</div>
  </body>
  
</html>