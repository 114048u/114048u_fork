<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Blog - Car Repair Shop Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css">
	<![endif]-->
</head>
<body>
<jsp:include page="Header.jsp" />
	
	<div id="body">
		<div class="content">
			<div class="section">
				<div class="blog" style ="padding:10px">
				
				<script src="js/jquery/jquery-1.7.1.min.js" type="text/javascript"></script>
<script type="text/javascript">
        var angles = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18];
        var current_angle_index = 5;
        var color_index = 0;
        var isLoading = false;
        var loaded_angles = [];
        var model_color_assets_path = 'honda-app/images/tablet/2014/accord-sedan/exterior-colors/';
        var colorArrowInitialPositionLeft = 0;

        function nextAngle() {
            if (current_angle_index == angles.length-1) 
                current_angle_index = 0;
            else
                current_angle_index++;
            return angles[current_angle_index];
        }

        function prevAngle() {
            if (current_angle_index == 0) 
                current_angle_index = angles.length-1;
            else
                current_angle_index--;
            return angles[current_angle_index];
        }
            
        function setImage(index) {
            if (!isLoading) {
                isLoading = true;
                var src = model_color_assets_path+vehicleColorCodeList[color_index]+'_'+index+'.jpg';
                $('#view-360 div img').attr('src', src).one('load', function() {
                        isLoading=false;
                    }).each(function() {
                      if(this.complete) $(this).load();
                    });
                }
        }
        
        
        $(document).ready(function() {
        

            var dragging = false
            var drag_threshold = 50;
            var init_mouse_x = 0;
            
            
            // touch/mouse events
            $(function() {
                var target = $('#view-360');
                
                if (!($.support.touch = 'ontouchend' in document)) {
                
                    $('#instructions').text('CLICK AND DRAG');
                
                    target.mousedown(function(e) {
                        e.preventDefault();
                        dragging = true;
                        init_mouse_x = e.pageX;
                    });
                    $(document).mouseup(function(e) {
                        e.preventDefault();
                        dragging = false
                    });
                    $(document).mousemove(function(e) {
                        e.preventDefault();
                        if (dragging) {

                            var mouse_x = e.pageX;
                            var mouse_y = e.pageY;
                            
                            var diff = mouse_x - init_mouse_x;
                            if (    (Math.abs(mouse_x - init_mouse_x)) > drag_threshold   ) {
                                init_mouse_x = mouse_x;
                                if (diff > 0) { 
                                    setImage(nextAngle());
                                } else {
                                    setImage(prevAngle());
                                }
                            }

                        }
                    });
                    
                    // support keyboard arrows
                    $(document).keydown(function(e) {
                        if (e.keyCode == 37) {
                            setImage(prevAngle());
                        }
                        if (e.keyCode == 39) {                            
                            setImage(nextAngle());
                        }
                    });
                    
                    
                } else {
                
                
                    $('#instructions').text('SWIPE TO ROTATE');

                    document.getElementById('view-360').addEventListener('touchstart', function(evt) {
                        //evt.preventDefault();                        
	                    _theTouch = evt.targetTouches[0];
	                    startX = _theTouch.pageX;
	                    startY = _theTouch.pageY;
                    } , true);

                    document.getElementById('view-360').addEventListener('touchmove', function(evt) {
    	                _theTouch = evt.targetTouches[0];
    	                _shiftX = _theTouch.pageX - startX;
    	                _shiftY = _theTouch.pageY - startY;
    	                var distance = 50;
            	
    	                var evalSlideX = (_shiftX<0)?(-_shiftX):_shiftX;
    	                var evalSlideY = (_shiftY<0)?(-_shiftY):_shiftY;
    	        
                        if (evalSlideY < evalSlideX)  {
                            evt.preventDefault();
            	
            	            if (_shiftX<-distance) setImage(prevAngle());
    	                    else if (_shiftX>distance) setImage(nextAngle());
                	
    	                    if ((_shiftX<-distance) || (_shiftX>distance)) {
    	                        startX = _theTouch.pageX;
    	                    }
    	                }
                    } , true);

                    document.getElementById('view-360').addEventListener('touchend', function(evt) {
                        // evt.preventDefault();
    	                _theTouch = evt.changedTouches[0];
    	                _shiftX = _theTouch.pageX - startX;
    	                _shiftY = _theTouch.pageY - startY;
                	
    	                dir = (_shiftX < 0) ? 'left' : 'right';
                	
    	                if ( Math.abs(_shiftX) >= ThresholdPixels ) {
    	                    if (dir == 'left') {
                                setImage(nextAngle());
    	                    } else if (dir == 'right') {
                                setImage(prevAngle());
    	                    }
    	                }
                    } , true);
                
                }
                
                
            });
            
            
            
            // resize width of color swatch container
            $('#colorsI').css('width' , (  $('#colorsI .colorSwatch').size() * 80  ) + 'px' );
            
            // move arrow over first color swatch            
            $('#colorArrow').css('margin-left' , getArrowPositionFromSwatchIndex(0) + 'px');
            
            
            $('#view-360 div img').css('margin-top' , '25px');
            
            
            // auto spin car
            var spinint = setInterval(function() {
                if (current_angle_index == 16) {
                    clearInterval(spinint);
                }
                setImage(nextAngle());
            } , 250);


            $('div.colorSwatch').on("click" , function() {
                $('#instruct span').hide();

                color_index = $('.colorSwatch').index($(this));
                $('#current-color').fadeOut(200 , function() {
                    $('#colorname').text(vehicleColorNameList[color_index]);
                }).fadeIn(200 , function() {


                    

                
                });;
                
                

                
                $('#view-360 div img').fadeOut(300 , function() {
                    $('#view-360 div img').attr('src', model_color_assets_path+vehicleColorCodeList[color_index]+'_'+angles[current_angle_index]+'.jpg');
                }).fadeIn();
                
                // move arrow over this color swatch                
                $('#colorArrow').animate( {                
                    'margin-left' : ( getArrowPositionFromSwatchIndex(color_index)) + 'px'
                } , 500);
                
                
                loadImages();
                // Tracking
                s.prop3 = vehicleColorNameList[color_index]
                s.linkTrackVars = 'prop1,prop2,prop3';
                CallSDotTL(s , true , 'EXTERIOR COLOR CLICK' , 'o');

            });
            
            
            $('div.colorSwatch').on("mouseenter" , function() {
                $(this).find('img').animate({ 'width' : '75px' , 'height' : '50px'  } , 200);
            });
            $('div.colorSwatch').on("mouseleave" , function() {
                $(this).find('img').animate({ 'width' : '70px' , 'height' : '45px'  } , 200);
            });
            
            loadImages();
        });
            
        var total_images = angles.length;
        var funcs = [];
        $('#image_loader').html('');
        function loadImages() {
        
            $.each(funcs, function(i) { clearTimeout(funcs[i]); });
            loaded_angles = [];
            funcs = [];
            total_images = angles.length;
                    
            loadMessage(total_images);
            $.each(angles, function(i) {
                funcs[i] = setTimeout(function() {
                    var src = model_color_assets_path+vehicleColorCodeList[color_index]+'_'+angles[i]+'.jpg';
                    $('<img src="'+src+'" />').appendTo($('#image_loader')).one('load', function() {
                            loaded_angles.push(src);
                            total_images--;
                            isDone(total_images);
                        }).each(function() {
                          if(this.complete) $(this).load();
                        });
                    }, 100*i);
            });
        }
        function isDone(total_images) {
            //$('#instruct span').show().text('Loading...'); //total_images
            loadMessage(total_images);
            if (loaded_angles.length == angles.length || total_images==0) {
                $('#instruct span').show();
                $('#instruct span').show().text('Swipe to rotate');
                }
        }
        
        function getArrowPositionFromSwatchIndex(idx) {
            var colorArrowInitialPositionLeft = (( $('#colors').width() - $('#colorsI').width() ) / 2)
            colorArrowInitialPositionLeft = colorArrowInitialPositionLeft + idx * ($('div.colorSwatch').eq(0).width() + 7);
            colorArrowInitialPositionLeft = colorArrowInitialPositionLeft + ($('div.colorSwatch').eq(0).width() / 2) ;
            colorArrowInitialPositionLeft = colorArrowInitialPositionLeft - 13;
            return colorArrowInitialPositionLeft;
        };             
                
        //var elipse = "Loading";
        //var elipse_count = 0;
        function loadMessage(total_images) {
        //    if (elipse_count< 3) {
        //        elipse += ".";
        //        elipse_count++;
        //        }
        //    else {
        //        elipse = "Loading" + total_images;
        //        elipse_count=0;
        //        }
            total_images = (total_images<0)?1:total_images;
            $('#instruct span').show().text("Loading "+total_images+" images");
        }
                        
    </script>
	
	    <!-- JavaScript -->
    <script type="text/javascript">
var vehicleColorNameList = new Array();
var vehicleColorCodeList = new Array();
vehicleColorNameList[0]="Basque Red Pearl II";
vehicleColorCodeList[0]="RE";
vehicleColorNameList[1]="Hematite Metallic";
vehicleColorCodeList[1]="HM";
vehicleColorNameList[2]="Obsidian Blue Pearl";
vehicleColorCodeList[2]="BL";
vehicleColorNameList[3]="Crystal Black Pearl";
vehicleColorCodeList[3]="BK";
vehicleColorNameList[4]="Modern Steel Metallic";
vehicleColorCodeList[4]="MS";
vehicleColorNameList[5]="White Orchid Pearl";
vehicleColorCodeList[5]="WH";
vehicleColorNameList[6]="Alabaster Silver Metallic";
vehicleColorCodeList[6]="SM";
vehicleColorNameList[7]="Champagne Frost Pearl";
vehicleColorCodeList[7]="BE";
</script>

	<!-- CSS -->
    
    <style type="text/css">
    
        /* Reset */
        html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, font, img, a img, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td {
    	    margin: 0px;
    	    padding: 0px;
    	    border: 0px;
    	    outline: 0px;
    	    list-style: none;
    	    text-decoration: none;
        }
        * {
    	    -webkit-touch-callout: none; /* prevent callout to copy image, etc when tap to hold */
    	    -webkit-text-size-adjust: none; /* prevent webkit from resizing text to fit */
        }
        body {
        }

    
    
        /* Colors */
        
        #wrapper {
        	background-color: #ffffff;
        	padding-bottom: 12px;
        	text-align: center;
        }
        #view-360 {
            margin: 0 auto;
            text-align:center;
            width: 797px;     
            cursor: e-resize;
        }
        #view-360 img {
            width: 797px;
            height: 405px;
        }
        #current-color {
            color: #000000;
            font-size: 12px;
            font-weight:bold;
            font-family: Helvetica, Arial, sans-serif;
            text-align: left;
            margin: 5px 0 10px 120px;
        }

        #colors { width: 797px; }
        
        #colorsI { margin: 0 auto; }
        
        #colors .colorSwatch { 
        	width:70px; 
        	height:45px; 
        	margin-right: 7px;
        	float: left;
        	cursor: pointer;
        }
        
        #instructions { font-size: 10px; font-weight: bold; }
            
        #image_loader { display:none; }
        
        #instruct { font-family: Arial; font-size:14px; font-weight:bold; color: #414042; text-align:center; margin-top:15px; height:20px; display: none; }
        #instruct span { display:none; }
        
        #colorArrow { margin: 0; padding: 0; text-align: left; }
        
        #color_disclaimer { clear: both; width: 500px; margin: 0 auto; padding-top: 10px; font-size: 12px; }
    </style>
				<div>
    <!-- HTML Content -->
    <div id="wrapper">
        <div id="instruct"><span></span></div>
      <div id="view-360">
        <div class="slider" id="view-1">
            <img src="honda-app/images/tablet/2014/accord-sedan/exterior-colors/RE_5.jpg" alt="" />
        </div>
    </div>
      
      <div id="instructions"></div>
      
      <div id="current-color">Select a color: <span id="colorname">Basque Red Pearl II</span></div>
      
      <div id="colors">
        <div id="colorArrow"><img src="images/_controls/models-exterior-360-colors-all/down-arrow.gif" /></div>
        <div id="colorsI"><div class="colorSwatch"><img src="images/tools/customize/colors/exterior_swatches/basque_red_pearl_ii.gif" /></div><div class="colorSwatch"><img src="../images/tools/customize/colors/exterior_swatches/hematite_metallic.gif" /></div><div class="colorSwatch"><img src="../images/tools/customize/colors/exterior_swatches/obsidian_blue_pearl.gif" /></div><div class="colorSwatch"><img src="../images/tools/customize/colors/exterior_swatches/crystal_black_pearl.gif" /></div><div class="colorSwatch"><img src="../images/tools/customize/colors/exterior_swatches/modern_steel_metallic.gif" /></div><div class="colorSwatch"><img src="../images/tools/customize/colors/exterior_swatches/white_orchid_pearl.gif" /></div><div class="colorSwatch"><img src="../images/tools/customize/colors/exterior_swatches/alabaster_silver_metallic.gif" /></div><div class="colorSwatch"><img src="../images/tools/customize/colors/exterior_swatches/champagne_frost_pearl.gif" /></div></div>
      </div>      
    </div>  

</div>  
				</div>
			</div>
			<div class="sidebar">
				<div class="post">
					
					
							<div>
				
							</div>
						
							<div>
								
							</div>
						
							<div>
							
					
							</div>
						</li>
						<li>
							
							<div>
						
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div>
			<div class="contact">
				<h3>contact information</h3>
				<ul>
					<li>
						<b>address:</b> <span>426 Grant Street colombo 10</span>
					</li>
					<li>
						<b>phone:</b> <span>0112222555</span>
					</li>
					<li>
						<b>fax:</b> <span>0112222555</span>
					</li>
					<li>
						<b>email:</b> <span><a href="http://www.bfc.com/misc/con">bfc@hotmail.com</a></span>
					</li>
				</ul>
			</div>
			<div class="tweets">
				<h3>recent tweets</h3>
				<ul>
					<li>
						<a href="#">Information technology Information technology<span>1 day ago</span></a>
					</li>
					<li>
						<a href="#">Information technology Information technology<span>2 days ago</span></a>
					</li>
				</ul>
			</div>
			<div class="posts">
				<h3>recent blog post</h3>
				<ul>
					<li>
						<a href="#">Information technology </a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology Information technology </a>
					</li>
				</ul>
			</div>
			<div class="connect">
				<h3>stay in touch</h3>
				<p>
					Information technology Information technology Information technology Information technology				</p>
				<ul>
					<li id="facebook">
						<a href="https://facebook.com">facebook</a>
					</li>
					<li id="twitter">
						<a href="https://twitter.com">twitter</a>
					</li>
					<li id="googleplus">
						<a href="https://google.com">googleplus</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="section">
			<p>
				&copy; BY MOROWING
			</p>
			<ul>
				<li>
					<a href="index.html">home</a>
				</li>
				<li>
					<a href="about.html">about</a>
				</li>
				<li>
					<a href="services.html">services</a>
				</li>
				<li>
					<a href="blog.html">blog</a>
				</li>
				<li>
					<a href="contact.html">contact</a>
				</li>
				<li>
					<a href="booking.html">book an appointment</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>