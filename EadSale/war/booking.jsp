<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Book An Apppointment - Car Repair Shop Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css">
	<![endif]-->
</head>
<body>
<jsp:include page="Header.jsp" />
	
	<div id="body">
		<div class="content">
			<div class="section">
				<div class="booking">
					<form action="itemmgtsevlet">
						<h4>Enter Car deatles to teh data store</h4>
						<div class="form1">
							<label for="fname"> <span>Car name</span>
								<input type="text" name="Cname" id="fname">
							</label>
							<label for="lname"> prise
								<input type="text" name="prise" id="lname">
							</label>
							<label for="email3"> <span>&nbsp;Catogory</span>
								<input type="text" name="prise" id="email3">
							</label>
							<label for="phone"> Quntity
								<input type="text" name="quont" id="phone">
							</label>
							<label for="address1"> Chasy Number<input type="text" name="cNumber" id="address1">
							</label>
							<div>
								<label for="city"> <span>city</span>
									<select name="city" id="city">
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
									</select>
								</label>
								<label for="state"> <span>state</span>
									<select name="state" id="state">
										<option value=""></option>
										<option value=""></option>
										<option value=""></option>
									</select>
								</label>
							</div>
							<label for="address2"> Engeen Numaber
								<input type="text" name="ENumber" id="address2">
							</label>
							<label for="zip"> <span>zipcode</span>
								<input type="text" name="zip" id="zip">
							</label>
							<label for="schedule"> <span>When Do You Prefer Us To Call?  (We only call during weekdays and Saturdays)</span>
								<select name="schedule" id="schedule">
									<option value=""></option>
									<option value="">In the morning (Between 9am - 12nn)</option>
									<option value="">In the afternoon (Between 1pm - 5pm)</option>
									<option value="">In the evening (Between 6pm-7pm)</option>
								</select>
							</label>
						</div>
						<h4>fill in details about your vehicle</h4>
						<div class="form2">
							<div>
								<label for="brand"> <span>brand</span>
									<input type="text" name="brand" id="brand">
								</label>
								<label for="model"> <span>model</span>
									<input type="text" name="model" id="model">
								</label>
								<label for="year"> <span>year</span>
									<input type="text" name="year" id="year">
								</label>
								<label for="mileage"> <span>estimated mileage</span>
									<input type="text" name="mileage" id="mileage">
								</label>
							</div>
							<label for="confirm" id="confirm2"> <span>If needed, are you ok with leaving your vehicle for the day?</span>
								<input type="text" name="confirm" id="confirm">
							</label>
							<label for="message2"> <span>Leave us a short message regarding your concerns and needs.</span>
								<textarea name="message2" id="message2" cols="30" rows="10"></textarea>
							</label>
						</div>
						<input type="submit" name="send2" id="send2" value="">
					</form>
				</div>
			</div>
			<div class="sidebar">
				<div class="contact">
					<div>
						<a href="gallery.html"><img src="images/lava2.jpg" alt=""></a>
					</div>
					<h4>contact information</h4>
					<ul>
						<li>
							<b>address:</b> <span>426 Grant Street Pine Colombo 
							10</span>
						</li>
						<li>
							<b>phone:</b> <span>0112222555</span>
						</li>
						<li>
							<b>fax:</b> <span>0112222555</span>
						</li>
						<li>
							<b>email:</b> <span><a href="http://bfc.com/misc/contact">bfc@hotmail.com</a></span>
						</li>
					</ul>
				</div>
				<div class="featured">
					<h3>featured services</h3>
					<ul>
						<li>
							<a href="services.html">engine maintenance</a>
						</li>
						<li>
							<a href="services.html">wheel alignment</a>
						</li>
						<li>
							<a href="services.html">air condition services</a>
						</li>
						<li>
							<a href="services.html">transmission</a>
						</li>
						<li>
							<a href="services.html">promos &amp; discounts</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div>
			<div class="contact">
				<h3>contact information</h3>
				<ul>
					<li>
						<b>address:</b> <span>426 Grant Street colombo 10</span>
					</li>
					<li>
						<b>phone:</b> <span>0112222555</span>
					</li>
					<li>
						<b>fax:</b> <span>0112222555</span>
					</li>
					<li>
						<b>email:</b> <span><a href="http://www.bfc.com/misc/con">bfc@hotmail.com</a></span>
					</li>
				</ul>
			</div>
			<div class="tweets">
				<h3>recent tweets</h3>
				<ul>
					<li>
						<a href="#">Information technology Information technology<span>1 day ago</span></a>
					</li>
					<li>
						<a href="#">Information technology Information technology<span>2 days ago</span></a>
					</li>
				</ul>
			</div>
			<div class="posts">
				<h3>recent blog post</h3>
				<ul>
					<li>
						<a href="#">Information technology </a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology Information technology </a>
					</li>
				</ul>
			</div>
			<div class="connect">
				<h3>stay in touch</h3>
				<p>
					Information technology Information technology Information technology Information technology				</p>
				<ul>
					<li id="facebook">
						<a href="https://facebook.com">facebook</a>
					</li>
					<li id="twitter">
						<a href="https://twitter.com">twitter</a>
					</li>
					<li id="googleplus">
						<a href="https://google.com">googleplus</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="section">
			<p>
				&copy; BY MOROWING
			</p>
			<ul>
				<li>
					<a href="index.html">home</a>
				</li>
				<li>
					<a href="about.html">about</a>
				</li>
				<li>
					<a href="services.html">services</a>
				</li>
				<li>
					<a href="blog.html">blog</a>
				</li>
				<li>
					<a href="contact.html">contact</a>
				</li>
				<li>
					<a href="booking.html">book an appointment</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>