<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>About - Car Repair Shop Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css">
	<![endif]-->
</head>
<body>
<jsp:include page="Header.jsp" />
	
	<div id="body">
		<div class="content">
			<div class="section">
				<div class="breadcrumb">
					<span>You are here:</span>
					<ul>
						<li>
							<a href="index.html">home</a>
						</li>
						<li>
							<a href="about.html">about us</a>
						</li>
						<li>
							<a href="team.html">the team</a>
						</li>
					</ul>
				</div>
				<div class="team">
					<h2>the team</h2>
					<div>
						<a href="#" class="figure"><img src="images/chuck.jpg" alt=""></a>
						<h3>Dismika</h3>
						<span>CEO/Owner</span>
						</div>
					<ul>
						<li>
							<a href="#" class="figure"><img src="images/robert.jpg" alt=""></a>
							<h3>Gihanz</h3>
							<span>Head Mechanic</span>
							</li>
						<li>
							<a href="#" class="figure"><img src="images/gregory.jpg" alt=""></a>
							<h3><a href="#">Prabad</a></h3>
							<span>GM</span>
						</li>
					</ul>
				</div>
			</div>
			<div class="sidebar">
				<div class="navigation">
					<h3>about</h3>
					<ul>
						<li>
							<a href="about.html">the company</a>
						</li>
						<li class="selected">
							<a href="team.html">the team</a>
						</li>
						<li>
							<a href="testimonials.html">testimonials</a>
						</li>
						<li>
							<a href="gallery.html">gallery</a>
						</li>
					</ul>
				</div>
				<div class="post">
					<h3>popular blog posts</h3>
					<ul>
						<li>
							<a href="blog-single.html" class="figure"><img src="images/girl-calling.jpg" alt=""></a>
							<div>
								<span><a href="blog-single.html">Head</a></span>
								<p>
									Information technology
									Information technology
								</p>
							</div>
						</li>
						<li>
							<a href="blog-single.html" class="figure"><img src="images/man2.jpg" alt=""></a>
							<div>
								<span><a href="blog-single.html">head</a></span>
								<p>
									Information technology								</p>
							</div>
						</li>
						<li>
							<a href="blog-single.html" class="figure"><img src="images/gentleman.jpg" alt=""></a>
							<div>
								<span><a href="blog-single.html">head</a></span>
								<p>
									Information technology								</p>
							</div>
						</li>
						<li>
							<a href="blog-single.html" class="figure"><img src="images/castle2.jpg" alt=""></a>
							<div>
								<span><a href="blog-single.html">head</a></span>
								<p>
									Information technology								</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
<div id="footer">
		<div>
			<div class="contact">
				<h3>contact information</h3>
				<ul>
					<li>
						<b>address:</b> <span>426 Grant Street colombo 10</span>
					</li>
					<li>
						<b>phone:</b> <span>0112222555</span>
					</li>
					<li>
						<b>fax:</b> <span>0112222555</span>
					</li>
					<li>
						<b>email:</b> <span><a href="http://www.bfc.com/misc/con">bfc@hotmail.com</a></span>
					</li>
				</ul>
			</div>
			<div class="tweets">
				<h3>recent tweets</h3>
				<ul>
					<li>
						<a href="#">Information technology Information technology<span>1 day ago</span></a>
					</li>
					<li>
						<a href="#">Information technology Information technology<span>2 days ago</span></a>
					</li>
				</ul>
			</div>
			<div class="posts">
				<h3>recent blog post</h3>
				<ul>
					<li>
						<a href="#">Information technology </a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology Information technology </a>
					</li>
				</ul>
			</div>
			<div class="connect">
				<h3>stay in touch</h3>
				<p>
					Information technology Information technology Information technology Information technology				</p>
				<ul>
					<li id="facebook">
						<a href="https://facebook.com">facebook</a>
					</li>
					<li id="twitter">
						<a href="https://twitter.com">twitter</a>
					</li>
					<li id="googleplus">
						<a href="https://google.com">googleplus</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="section">
			<p>
				&copy; BY MOROWING
			</p>
			<ul>
				<li>
					<a href="index.html">home</a>
				</li>
				<li>
					<a href="about.html">about</a>
				</li>
				<li>
					<a href="services.html">services</a>
				</li>
				<li>
					<a href="blog.html">blog</a>
				</li>
				<li>
					<a href="contact.html">contact</a>
				</li>
				<li>
					<a href="booking.html">book an appointment</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>