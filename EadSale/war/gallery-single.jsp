<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>About - Car Repair Shop Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css">
	<![endif]-->
</head>
<body>
<jsp:include page="Header.jsp" />
	
	<div id="body">
		<div class="content">
			<div class="section">
				<div class="breadcrumb">
					<span>You are here:</span>
					<ul>
						<li>
							<a href="index.html">home</a>
						</li>
						<li>
							<a href="about.html">about us</a>
						</li>
						<li>
							<a href="gallery.html">gallery</a>
						</li>
					</ul>
				</div>
				<div class="gallery">
					<h2>Aenean interdum erat urna</h2>
					<a href="#" class="figure"><img src="images/car3.jpg" alt=""></a>
					<div>
						<span class="paging"><a href="#" class="prev">prev</a><span>1/4</span><a href="#" class="next">next</a></span>
						<p>
							information</p>
					</div>
				</div>
			</div>
			<div class="sidebar">
				<div class="navigation">
					<h3>about</h3>
					<ul>
						<li>
							<a href="about.html">the company</a>
						</li>
						<li>
							<a href="team.html">the team</a>
						</li>
						<li>
							<a href="testimonials.html">testimonials</a>
						</li>
						<li class="selected">
							<a href="gallery.html">gallery</a>
						</li>
					</ul>
				</div>
				latest blog posts
 head information technologyi nformation technology information technology
 head information technology information technology information technology. 
 head information technologyinformation technology 
 head information technologyinformation technology. 
				<div class="gallery">
					<h3>featured from gallery</h3>
					<a href="gallery.html" class="figure"><img src="images/featured.jpg" alt=""></a> <span><a href="gallery.html">Aenean interdum erat urna</a></span>
					<p>
						information</p>
				</div>
			</div>
		</div>
	</div>
	<div id="footer">
		<div>
			<div class="contact">
				<h3>contact information</h3>
				<ul>
					<li>
						<b>address:</b> <span>426 Grant Street colombo 10</span>
					</li>
					<li>
						<b>phone:</b> <span>0112222555</span>
					</li>
					<li>
						<b>fax:</b> <span>0112222555</span>
					</li>
					<li>
						<b>email:</b> <span><a href="http://www.bfc.com/misc/con">bfc@hotmail.com</a></span>
					</li>
				</ul>
			</div>
			<div class="tweets">
				<h3>recent tweets</h3>
				<ul>
					<li>
						<a href="#">Information technology Information technology<span>1 day ago</span></a>
					</li>
					<li>
						<a href="#">Information technology Information technology<span>2 days ago</span></a>
					</li>
				</ul>
			</div>
			<div class="posts">
				<h3>recent blog post</h3>
				<ul>
					<li>
						<a href="#">Information technology </a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology Information technology </a>
					</li>
				</ul>
			</div>
			<div class="connect">
				<h3>stay in touch</h3>
				<p>
					Information technology Information technology Information technology Information technology				</p>
				<ul>
					<li id="facebook">
						<a href="https://facebook.com">facebook</a>
					</li>
					<li id="twitter">
						<a href="https://twitter.com">twitter</a>
					</li>
					<li id="googleplus">
						<a href="https://google.com">googleplus</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="section">
			<p>
				&copy; BY MOROWING
			</p>
			<ul>
				<li>
					<a href="index.html">home</a>
				</li>
				<li>
					<a href="about.html">about</a>
				</li>
				<li>
					<a href="services.html">services</a>
				</li>
				<li>
					<a href="blog.html">blog</a>
				</li>
				<li>
					<a href="contact.html">contact</a>
				</li>
				<li>
					<a href="booking.html">book an appointment</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>