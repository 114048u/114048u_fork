<!DOCTYPE HTML>
<!-- Website Template by freewebsitetemplates.com -->
<html>
<head>
	<meta charset="UTF-8">
	<title>Services - Car Repair Shop Website Template</title>
	<link rel="stylesheet" href="css/style.css" type="text/css">
	<!--[if lt IE 9]>
		<link rel="stylesheet" type="text/css" href="css/ie.css">
	<![endif]-->
</head>
<body>
<jsp:include page="Header.jsp" />
	
	<div id="body">
		<div class="content">
			<div class="section">
				<div class="breadcrumb">
					<span>You are here:</span>
					<ul>
						<li>
							<a href="index.html">home</a>
						</li>
						<li>
							<a href="services.html">services</a>
						</li>
						<li>
							<a href="services.html">engine maintenance</a>
						</li>
					</ul>
				</div>
				<div class="services">
					<h2>services</h2>
				</div><div><script type="text/javascript" src="https://apis.google.com/js/platform.js"></script> <div style="height:50px;" class="g-hangout" data-render="createhangout"></div>
			</div>
			<div class="sidebar">
				<div class="navigation">
					<h3>Services</h3>
					<ul>
						<li class="selected">
							<a href="services.html">engine maintenance</a>
						</li>
						<li>
							<a href="services.html">wheel alignment</a>
						</li>
						<li>
							<a href="services.html">air condition services</a>
						</li>
						<li>
							<a href="services.html">transmission</a>
						</li>
						<li>
							<a href="promo.html">promos &amp; discounts</a>
						</li>
					</ul>
				</div>
<div class="contact">
					<div>
						<a href="gallery.html"><img src="images/lava2.jpg" alt=""></a>
					</div>
					<h4>contact information</h4>
					<ul>
						<li>
							<b>address:</b> <span>426 Grant Street colombo 10</span>
						</li>
						<li>
							<b>phone:</b> <span>0112222555</span>
						</li>
						<li>
							<b>fax:</b> <span>0112222555</span>
						</li>
						<li>
							<b>email:</b> <span><a href="http://www.bfc.com/misc/contact">bfc@hotmail.com</a></span>
						</li>
					</ul>
				</div>
				<div class="gallery">
					<h3>featured from gallery</h3>
					<a href="gallery.html" class="figure"><img src="images/featured.jpg" alt=""></a> <span><a href="gallery.html">Aenean interdum erat urna</a></span>
					<p>
						information</p>
				</div>
			</div>
		</div>
	</div>
<div id="footer">
		<div>
			<div class="contact">
				<h3>contact information</h3>
				<ul>
					<li>
						<b>address:</b> <span>426 Grant Street colombo 10</span>
					</li>
					<li>
						<b>phone:</b> <span>0112222555</span>
					</li>
					<li>
						<b>fax:</b> <span>0112222555</span>
					</li>
					<li>
						<b>email:</b> <span><a href="http://www.bfc.com/misc/con">bfc@hotmail.com</a></span>
					</li>
				</ul>
			</div>
			<div class="tweets">
				<h3>recent tweets</h3>
				<ul>
					<li>
						<a href="#">Information technology Information technology<span>1 day ago</span></a>
					</li>
					<li>
						<a href="#">Information technology Information technology<span>2 days ago</span></a>
					</li>
				</ul>
			</div>
			<div class="posts">
				<h3>recent blog post</h3>
				<ul>
					<li>
						<a href="#">Information technology </a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology</a>
					</li>
					<li>
						<a href="#">Information technology Information technology </a>
					</li>
				</ul>
			</div>
			<div class="connect">
				<h3>stay in touch</h3>
				<p>
					Information technology Information technology Information technology Information technology				</p>
				<ul>
					<li id="facebook">
						<a href="https://facebook.com">facebook</a>
					</li>
					<li id="twitter">
						<a href="https://twitter.com">twitter</a>
					</li>
					<li id="googleplus">
						<a href="https://google.com">googleplus</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="section">
			<p>
				&copy; BY MOROWING
			</p>
			<ul>
				<li>
					<a href="index.html">home</a>
				</li>
				<li>
					<a href="about.html">about</a>
				</li>
				<li>
					<a href="services.html">services</a>
				</li>
				<li>
					<a href="blog.html">blog</a>
				</li>
				<li>
					<a href="contact.html">contact</a>
				</li>
				<li>
					<a href="booking.html">book an appointment</a>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>